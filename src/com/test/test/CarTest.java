package com.test.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.test.car.Car;

public class CarTest {

	@Test
	public void testGetModel(){
		String expectedResult = "Opel";
		
		Car Car = new Car("Opel", "Grey");
		String actualResult = Car.getModel();
		
		Assert.assertEquals(actualResult, expectedResult);
	}
	
	@Test
	public void testGetColor(){
		String expectedResult = "Grey";
		Car Car1 = new Car("Opel", "Grey");
		String actualResult = Car1.getColor();
		
		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test
	public void testChangeColor(){
		String expectedResult = "Green";
		
		Car Car2 = new Car("Ford", "Black");
		Car2.changeColor("Green");
		String actualResult = Car2.getColor();
		
		Assert.assertEquals(actualResult, expectedResult);
	}
	
	@Test
	public void testChangeReceivedColor(){
		String expectedResult = "White";
		
		Car Car3 = new Car("Skoda", "White");
		String SkodaColor = Car3.getColor();
		
		Car Car4 = new Car("Volvo", "Red");
		Car4.changeColor(SkodaColor);
		String actualResult = Car4.getColor();
		
		Assert.assertEquals(actualResult, expectedResult);
	}
}
